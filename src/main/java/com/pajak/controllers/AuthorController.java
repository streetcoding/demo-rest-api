package com.pajak.controllers;

import com.pajak.models.entities.Author;
import com.pajak.models.repositories.AuthorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/authors")
public class AuthorController {
    
    @Autowired
    private AuthorRepository authorRepository;
    
    @PostMapping
    public Author create(@RequestBody Author author){
        return authorRepository.save(author);
    }

    @GetMapping
    public Iterable<Author> getAll(){
        return authorRepository.findAll();
    }
}
