package com.pajak.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.pajak.dto.BookData;
import com.pajak.dto.ResponseData;
import com.pajak.dto.SearchData;
import com.pajak.models.entities.Book;
import com.pajak.models.repositories.AuthorRepository;
import com.pajak.models.repositories.BookRepository;
import com.pajak.models.repositories.CategoryRepository;
import com.pajak.utilities.ErrorsParsing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private CategoryRepository categoryRepo;
    @Autowired
    private AuthorRepository authorRepo;

    @PostMapping
    public ResponseEntity<ResponseData> createBook(@Valid @RequestBody BookData book, Errors errors) {

        ResponseData response = new ResponseData();
        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorsParsing.parse(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        try {
            Book newBook = new Book();
            newBook.setTitle(book.getTitle());
            newBook.setDescription(book.getDescription());
            newBook.setAuthor(authorRepo.findById(book.getAuthorId()).get());
            newBook.setCode(book.getCode());
            newBook.setCategory(categoryRepo.findById(book.getCategoryId()).get());

            response.setPayload(bookRepository.save(newBook));
            response.setStatus(true);
            response.getMessages().add("Book saved!");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<ResponseData> findAll(@PathVariable("page") int page, @PathVariable("size") int size) {
        ResponseData response = new ResponseData();
        try {
            Pageable pageable = PageRequest.of(page-1, size, Sort.by("title").descending());
            response.setPayload(bookRepository.findAll(pageable));
            response.setStatus(true);
            response.getMessages().add("List of Book");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData> findOne(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();
        try {
            Optional<Book> book = bookRepository.findById(id);
            if (book.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(book.get());
            response.setStatus(true);
            response.getMessages().add("Book found");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping("/search")
    public ResponseEntity<ResponseData> findByTitle(@RequestBody SearchData searchData) {
        ResponseData response = new ResponseData();
        try {
            List<Book> books = bookRepository.findByTitleContains(searchData.getKey());
            if (books.size() < 1) {
                response.setStatus(false);
                response.getMessages().add("Book with title \'" + searchData.getKey() + "\' not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(books);
            response.setStatus(true);
            response.getMessages().add("Book found");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData> update(@PathVariable("id") Long id, @Valid @RequestBody BookData book,
            Errors errors) {
        ResponseData response = new ResponseData();
        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorsParsing.parse(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        try {
            Optional<Book> existingBook = bookRepository.findById(id);
            if (existingBook.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            existingBook.get().setTitle(book.getTitle());
            existingBook.get().setDescription(book.getDescription());
            existingBook.get().setAuthor(authorRepo.findById(book.getAuthorId()).get());
            existingBook.get().setCode(book.getCode());
            bookRepository.save(existingBook.get());

            response.setPayload(existingBook.get());
            response.setStatus(true);
            response.getMessages().add("Book updated!");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData> removeBook(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();
        try {
            Optional<Book> existingBook = bookRepository.findById(id);
            if (existingBook.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            bookRepository.delete(existingBook.get());
            response.setStatus(true);
            response.getMessages().add("Book with id " + id + " deleted");
            return ResponseEntity.ok(response);
        }catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<ResponseData> findByCategory(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();
        try {
            List<Book> books = bookRepository.findByCategoryId(id);
            if (books.size() < 1) {
                response.setStatus(false);
                response.getMessages().add("Book with category id \'" + id + "\' not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(books);
            response.setStatus(true);
            response.getMessages().add("Book found");
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

}
