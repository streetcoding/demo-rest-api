package com.pajak.controllers;

import com.pajak.models.entities.Category;
import com.pajak.models.repositories.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    
    @Autowired
    private CategoryRepository categoryRepo;

    @PostMapping
    public Category create(@RequestBody Category category){
        return categoryRepo.save(category);
    }

    @GetMapping
    public Iterable<Category> findAll(){
        return categoryRepo.findAll();
    }
}
