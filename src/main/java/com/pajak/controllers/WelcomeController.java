package com.pajak.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class WelcomeController {
    
    @GetMapping
    public Map<String, String> welcome(){
        Map<String, String> response = new HashMap<>();
        response.put("greetings", "Hello World!");
        return response;
    }
}
