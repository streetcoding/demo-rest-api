package com.pajak.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class BookData {
    
    @NotEmpty(message = "Code is required")
    @Size(min = 3, max = 5, message = "Code length must be 3 to 5 characters")
    @Pattern(regexp ="BK[0-9]+", message = "Code must be start with BK")
    private String code;

    @NotEmpty(message = "Title is required")
    private String title;

    private String description;
    
    @NotEmpty(message = "Author is required")
    private String authorId;

    private Long categoryId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthor(String authorId) {
        this.authorId = authorId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategory(Long categoryId) {
        this.categoryId = categoryId;
    }

    
}
