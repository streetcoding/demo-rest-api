package com.pajak.models.repositories;

import com.pajak.models.entities.Author;

import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, String> {
    
}
